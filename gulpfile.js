/**
 * Created by mrdingo on 19.3.17.
 */
var gulp = require('gulp');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-clean-css');
var del = require('del');

gulp.task('build', function () {
    gulp.src(['app/*.html'])
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulp.dest('public'));

    gulp.src(['app/js/modules/**/*.html'])
        .pipe(gulp.dest('public/js/modules'));


});

gulp.task('clean', function() {
    return del(['public']);
});


gulp.task('heroku', function () {
    gulp.src(['app/*.html'])
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(gulp.dest('public/app'));

    gulp.src(['app/js/modules/**/*.html'])
        .pipe(gulp.dest('public/app/js/modules'));

    gulp.src(['index.js'])
        .pipe(gulp.dest('public'));
    gulp.src(['package.json'])
        .pipe(gulp.dest('public'));
    gulp.src(['Procfile'])
        .pipe(gulp.dest('public'));
});