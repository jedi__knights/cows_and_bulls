/**
 * Created by mrdingo on 18.3.17.
 */

var visual = (function () {
    var i = 0;
    function init(func) {
        i = 0;
        $('#game-area').load('/js/modules/visual/game.tmpl.html',function () {
            $('#game-button').click(func)
        });

    }
    function setInfoStep(value){
        $('#history-input').append('<tr><td>' + (++i) +
                                   '</td><td>' +  $('#game input').val() +
                                   '</td><td>'  + value.bulls +
                                   '</td><td>'  + value.cows+'</td></tr>');
    }
    function getInputValue(){
        return $('#game input').val();
    }
    function setOnClick(func) {
        $('#game-button').click(func);
    }
    function willGame() {
        //$('#input-area').append("u will");
        $('#game-area').load('/js/modules/visual/game.will.html');
    }
    function loseGame() {
        //$('#input-area').append("u lose");
        $('#game-area').load('/js/modules/visual/game.lose.html');

    }
    return {
        init:init,
        setInfoStep:setInfoStep,
        getInputValue: getInputValue,
        activeStepButton:setOnClick,
        willGame:willGame,
        loseGame:loseGame
    }
})();