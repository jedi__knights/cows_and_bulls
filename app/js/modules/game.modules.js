/**
 * Created by mrdingo on 18.3.17.
 */

var game = (function () {
    var generationArray = [];
    var countBc;
    var stepCount;
    var maxStep;
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    function getMaxStepForGame() {
        return maxStep;
    }
    function init(level) {
        countBc =0;
        stepCount = 0;
        maxStep = getMaxStep(level);
        generationArray = generateDigit();
        return generationArray;
    }
    function getMaxStep(level) {
        var max =0;
        switch (level){
            case "easy": {
                max = 15;
                break;
            }
            default: {
                max = 100;
                break;
            }
        }
        return max;
    }
    function generateDigit() {
        var a = [4];
        for (var i = 0; i < 4; i++) {
            var flag = 0;
            do {
                flag = 0;
                var tmp = getRandomInt(0, 9);
                for (var j = 0; j < i; j++) {
                    if (tmp == a[j]) flag = 1;
                }
                if(tmp == 0 && i == 0) flag = 1;
                if (flag == 0) a[i] = tmp;

            } while (flag == 1);
        }
        return a;
    }
    function bccount(i){
            var cows = 0, bulls = 0;
            var a = [3];
            var j = 3;
            while(i != 0)
            {
                var tmp = i % 10;
                a[j] = tmp;
                i /= 10;
                i = Math.floor(i);
                j--;
            }
            var t  = 0;
            for(j = 0; j < 4; j++)
            {
                for(var k = 0; k < 4; k++)
                {
                    if(a[j] == a[k])
                    {
                       t++;
                    }
                }
            }
            if(t == 4)
            {
                for (var k = 0; k < 4; k++)
                {
                    for (var j = 0; j < 4; j++)
                        if (generationArray[k] == a[j] && k == j)
                            bulls++;
                        else if (generationArray[k] == a[j])
                            cows++;

                }
            }
        stepCount++;
        countBc = {
            bulls: bulls,
            cows: cows
        };

        return countBc;
    }
    function getStepCount() {
        return stepCount;
    }
    function  isEndGame() {
        return (countBc.bulls ===  4);
    }
    return {
        init: init,
        bccount: bccount,
        isEndGame:isEndGame,
        getStepCount:getStepCount,
        getMaxSteps:getMaxStepForGame
    }
})();