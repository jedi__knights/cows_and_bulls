/**
 * Created by mrdingo on 18.3.17.
 */

var fasade = (function () {

    var gameStep = function () {
        visual.setInfoStep(
            game.bccount(
                visual.getInputValue())
        );
        if(game.isEndGame())
            visual.willGame();
        else
            if(game.getStepCount() == game.getMaxSteps())
                visual.loseGame();
    };

    function init() {
        var level = 'easy';
        game.init(level);
        visual.init(gameStep);
    }
    return {
        init:init
    }
})();
fasade.init();
