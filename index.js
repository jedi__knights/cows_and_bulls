/**
 * Created by mrdingo on 18.3.17.
 */

var express = require('express');
var app = express();
app.use('/', express.static(__dirname + '/app'));
var port = process.env.PORT || 9999;
app.listen(port, function() {
    console.log("Listening on " + port);
});