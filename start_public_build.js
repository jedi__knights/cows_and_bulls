/**
 * Created by mrdingo on 26.3.17.
 */
var express = require('express');
var app = express();
app.use('/', express.static(__dirname + '/public'));
var port = process.env.PORT || 9999;
app.listen(port, function() {
    console.log("Listening on " + port);
});